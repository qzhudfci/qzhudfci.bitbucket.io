var colorlist = [
    "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
    "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
    "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
    "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
    "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
    "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
    "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
    "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
    "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
    "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
    "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
    "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
    "#5B4534", "#FDE8DC",
    "#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
    "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
    "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
    "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
    "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
    "#372101"
];

function do_fetch(arg_1, is_by_gene){

//fetch("processed.anderson.pos1.txt")
fetch(arg_1)
.then(response => response.text())
.then(function(text){
	var pointlist2 = text.split("\n");
	by_gene = {};
	by_cell = {};
	for(i=0; i<pointlist2.length-1; i++){
		var newplist = pointlist2[i].split(",");
		x = Number(newplist[2]);
		y = Number(newplist[3]);
		z = Number(newplist[4]);
		cell = Number(newplist[1]);
		gene = newplist[0];
		if(!by_gene.hasOwnProperty(gene)){
			by_gene[gene] = {"x":[], "y":[], "z":[]};
		}
		by_gene[gene]["x"].push(x);
		by_gene[gene]["y"].push(y);
		by_gene[gene]["z"].push(z);

		if(!by_cell.hasOwnProperty(cell)){
			by_cell[cell] = {"x":[], "y":[], "z":[]};
		}
		by_cell[cell]["x"].push(x);
		by_cell[cell]["y"].push(y);
		by_cell[cell]["z"].push(z);
	}

	var data = [];
	
	var g_id = 0;
	var c_id = 0;
	if(is_by_gene){
	Object.keys(by_gene).forEach(function(key){
		var trace1 = {
		 x:by_gene[key]["x"], y:by_gene[key]["y"], z:by_gene[key]["z"],
		 mode:"markers",
		 name:key + " (" + String(by_gene[key]["x"].length + ")"),
		 marker: {color: colorlist[g_id % 50], size:1, symbol:"circle", opacity:0.8,},
		 type: "scatter3d"
		};
		data.push(trace1);
		g_id+=1;
	});
	}else{
	Object.keys(by_cell).forEach(function(key){
		var trace1 = {
		 x:by_cell[key]["x"], y:by_cell[key]["y"], z:by_cell[key]["z"],
		 mode:"markers",
		 name:"Cell " + key + " (" + String(by_cell[key]["x"].length + ")"),
		 marker: {color: colorlist[c_id % 50], size:1, symbol:"circle", opacity:0.8,},
		 type: "scatter3d"
		};
		data.push(trace1);
		c_id+=1;
	});
	}
	var layout = {
    scene:{
        aspectmode:"manual",
        aspectratio:{x:1,y:1, z:0.05,}
    },
    dragmode:false,
    margin:{l:0, r:0, b:0, t:0},
    };
	Plotly.newPlot("myDiv", data, layout, {showSendToCloud:true});

});
}
