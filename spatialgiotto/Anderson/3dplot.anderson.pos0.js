
Plotly.d3.csv("processed.anderson.pos0.by.gene.txt", function(err, rows){
    function unpack(rows, key){
        return rows.map(function(row)
        {return row[key];});
    }

var data = [];


var trace1 = {
    x:unpack(rows, "x1"), y:unpack(rows, "y1"), z:unpack(rows, "z1"),
    mode:"markers",
    name:"Adarb2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x2"), y:unpack(rows, "y2"), z:unpack(rows, "z2"),
    mode:"markers",
    name:"Arhgap36",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x3"), y:unpack(rows, "y3"), z:unpack(rows, "z3"),
    mode:"markers",
    name:"Bdnf",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x4"), y:unpack(rows, "y4"), z:unpack(rows, "z4"),
    mode:"markers",
    name:"C1ql2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x5"), y:unpack(rows, "y5"), z:unpack(rows, "z5"),
    mode:"markers",
    name:"Calcrl",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x6"), y:unpack(rows, "y6"), z:unpack(rows, "z6"),
    mode:"markers",
    name:"Cartpt",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x7"), y:unpack(rows, "y7"), z:unpack(rows, "z7"),
    mode:"markers",
    name:"Cbln2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x8"), y:unpack(rows, "y8"), z:unpack(rows, "z8"),
    mode:"markers",
    name:"Cbln4",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x9"), y:unpack(rows, "y9"), z:unpack(rows, "z9"),
    mode:"markers",
    name:"Cck",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x10"), y:unpack(rows, "y10"), z:unpack(rows, "z10"),
    mode:"markers",
    name:"Ctxn3",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x11"), y:unpack(rows, "y11"), z:unpack(rows, "z11"),
    mode:"markers",
    name:"Dlk1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x12"), y:unpack(rows, "y12"), z:unpack(rows, "z12"),
    mode:"markers",
    name:"Ecel1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x13"), y:unpack(rows, "y13"), z:unpack(rows, "z13"),
    mode:"markers",
    name:"Esr1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x14"), y:unpack(rows, "y14"), z:unpack(rows, "z14"),
    mode:"markers",
    name:"Fezf1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x15"), y:unpack(rows, "y15"), z:unpack(rows, "z15"),
    mode:"markers",
    name:"Fos",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x16"), y:unpack(rows, "y16"), z:unpack(rows, "z16"),
    mode:"markers",
    name:"Fosl2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x17"), y:unpack(rows, "y17"), z:unpack(rows, "z17"),
    mode:"markers",
    name:"Foxp2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x18"), y:unpack(rows, "y18"), z:unpack(rows, "z18"),
    mode:"markers",
    name:"Gad1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x19"), y:unpack(rows, "y19"), z:unpack(rows, "z19"),
    mode:"markers",
    name:"Gad2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x20"), y:unpack(rows, "y20"), z:unpack(rows, "z20"),
    mode:"markers",
    name:"Gal",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x21"), y:unpack(rows, "y21"), z:unpack(rows, "z21"),
    mode:"markers",
    name:"Gldn",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x22"), y:unpack(rows, "y22"), z:unpack(rows, "z22"),
    mode:"markers",
    name:"Gpc3",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x23"), y:unpack(rows, "y23"), z:unpack(rows, "z23"),
    mode:"markers",
    name:"Gpr101",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x24"), y:unpack(rows, "y24"), z:unpack(rows, "z24"),
    mode:"markers",
    name:"Gpr149",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x25"), y:unpack(rows, "y25"), z:unpack(rows, "z25"),
    mode:"markers",
    name:"Gpr83",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x26"), y:unpack(rows, "y26"), z:unpack(rows, "z26"),
    mode:"markers",
    name:"Gpr88",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x27"), y:unpack(rows, "y27"), z:unpack(rows, "z27"),
    mode:"markers",
    name:"Il1rapl2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x28"), y:unpack(rows, "y28"), z:unpack(rows, "z28"),
    mode:"markers",
    name:"Isl1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x29"), y:unpack(rows, "y29"), z:unpack(rows, "z29"),
    mode:"markers",
    name:"Kctd18",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x30"), y:unpack(rows, "y30"), z:unpack(rows, "z30"),
    mode:"markers",
    name:"Kirrel3",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x31"), y:unpack(rows, "y31"), z:unpack(rows, "z31"),
    mode:"markers",
    name:"Ldb2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x32"), y:unpack(rows, "y32"), z:unpack(rows, "z32"),
    mode:"markers",
    name:"Lypd1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x33"), y:unpack(rows, "y33"), z:unpack(rows, "z33"),
    mode:"markers",
    name:"Mc4r",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x34"), y:unpack(rows, "y34"), z:unpack(rows, "z34"),
    mode:"markers",
    name:"Necab1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x35"), y:unpack(rows, "y35"), z:unpack(rows, "z35"),
    mode:"markers",
    name:"Npy2r",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x36"), y:unpack(rows, "y36"), z:unpack(rows, "z36"),
    mode:"markers",
    name:"Nr2f2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x37"), y:unpack(rows, "y37"), z:unpack(rows, "z37"),
    mode:"markers",
    name:"Nr5a1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x38"), y:unpack(rows, "y38"), z:unpack(rows, "z38"),
    mode:"markers",
    name:"Ntng1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x39"), y:unpack(rows, "y39"), z:unpack(rows, "z39"),
    mode:"markers",
    name:"Nts",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x40"), y:unpack(rows, "y40"), z:unpack(rows, "z40"),
    mode:"markers",
    name:"Nup62cl",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x41"), y:unpack(rows, "y41"), z:unpack(rows, "z41"),
    mode:"markers",
    name:"Nxph1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x42"), y:unpack(rows, "y42"), z:unpack(rows, "z42"),
    mode:"markers",
    name:"Pdyn",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x43"), y:unpack(rows, "y43"), z:unpack(rows, "z43"),
    mode:"markers",
    name:"Plagl1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x44"), y:unpack(rows, "y44"), z:unpack(rows, "z44"),
    mode:"markers",
    name:"Rmst",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x45"), y:unpack(rows, "y45"), z:unpack(rows, "z45"),
    mode:"markers",
    name:"Rreb1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x46"), y:unpack(rows, "y46"), z:unpack(rows, "z46"),
    mode:"markers",
    name:"Satb1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x47"), y:unpack(rows, "y47"), z:unpack(rows, "z47"),
    mode:"markers",
    name:"Satb2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x48"), y:unpack(rows, "y48"), z:unpack(rows, "z48"),
    mode:"markers",
    name:"Sema3c",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x49"), y:unpack(rows, "y49"), z:unpack(rows, "z49"),
    mode:"markers",
    name:"Six3",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x50"), y:unpack(rows, "y50"), z:unpack(rows, "z50"),
    mode:"markers",
    name:"Slc17a6",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x51"), y:unpack(rows, "y51"), z:unpack(rows, "z51"),
    mode:"markers",
    name:"Slc32a1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x52"), y:unpack(rows, "y52"), z:unpack(rows, "z52"),
    mode:"markers",
    name:"Slc35f4",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x53"), y:unpack(rows, "y53"), z:unpack(rows, "z53"),
    mode:"markers",
    name:"Sorcs1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x54"), y:unpack(rows, "y54"), z:unpack(rows, "z54"),
    mode:"markers",
    name:"Sst",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x55"), y:unpack(rows, "y55"), z:unpack(rows, "z55"),
    mode:"markers",
    name:"Stxbp6",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x56"), y:unpack(rows, "y56"), z:unpack(rows, "z56"),
    mode:"markers",
    name:"Syndig1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x57"), y:unpack(rows, "y57"), z:unpack(rows, "z57"),
    mode:"markers",
    name:"Tac1",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x58"), y:unpack(rows, "y58"), z:unpack(rows, "z58"),
    mode:"markers",
    name:"Tcf7l2",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x59"), y:unpack(rows, "y59"), z:unpack(rows, "z59"),
    mode:"markers",
    name:"Unc5d",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);

var trace1 = {
    x:unpack(rows, "x60"), y:unpack(rows, "y60"), z:unpack(rows, "z60"),
    mode:"markers",
    name:"Vgf",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);


var layout = {
    scene:{
        aspectmode:"manual",
        aspectratio:{x:1,y:1, z:0.05,}
    },
    dragmode:false,
    margin:{l:0, r:0, b:0, t:0},
    };
Plotly.newPlot("myDiv", data, layout, {showSendToCloud:true});
});

