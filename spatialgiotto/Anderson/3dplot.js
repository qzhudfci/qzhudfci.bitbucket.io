Plotly.d3.csv("processed.anderson.pos0.by.gene.txt", function(err, rows){
	function unpack(rows, key){
		return rows.map(function(row)
		{return row[key];});
	}

var data = [];

var trace1 = {
	x:unpack(rows, "x1"), y:unpack(rows, "y1"), z:unpack(rows, "z1"),
	mode:"markers",
	name:"TR1",
	marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
	type: "scatter3d"
};

data.push(trace1);
var trace1 = {
	x:unpack(rows, "x2"), y:unpack(rows, "y2"), z:unpack(rows, "z2"),
	mode:"markers",
	name:"TR2",
	marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
	type: "scatter3d"
};
data.push(trace1);

var trace1 = {
	x:unpack(rows, "x3"), y:unpack(rows, "y3"), z:unpack(rows, "z3"),
	mode:"markers",
	name:"TR3",
	marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
	type: "scatter3d"
};
data.push(trace1);
var trace1 = {
	x:unpack(rows, "x4"), y:unpack(rows, "y4"), z:unpack(rows, "z4"),
	mode:"markers",
	name:"TR4",
	marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
	type: "scatter3d"
};
data.push(trace1);

//var data = [trace1];
var layout = {
	scene:{
		aspectmode:"manual",
		aspectratio:{x:1,y:1, z:0.05,}
	},
	dragmode:false,
	margin:{l:0, r:0, b:0, t:0}, 
	};
Plotly.newPlot("myDiv", data, layout, {showSendToCloud:true});
});
