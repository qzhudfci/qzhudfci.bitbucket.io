#!/usr/bin/python

import sys
import re
import os

f = open(sys.argv[1])
h = f.readline().rstrip("\n").split(",")[1:]
tx = {}
#sys.stdout.write("gene,x1,y1,z1\n")
while True:
	l = f.readline()
	if l=="": break
	l = l.rstrip("\n")
	ll = l.split(",")
	gene = ll[0]
	for x in ll[1:]:
		if x=="0":
			continue
		m = re.match("\[(.*)\]", x)
		if m is not None:
			mx = m.group(1).split(";")
			for mi in mx:
				mi_s = mi.split(" ")
				#sys.stdout.write("%s,%s,%s,%s\n" % (gene, mi_s[0], mi_s[1], mi_s[2]))
				tx.setdefault(gene, [])
				tx[gene].append([mi_s[0], mi_s[1], mi_s[2]])
f.close()

fw = open(sys.argv[2], "w")

for t_id, t_gene in enumerate(tx.keys()):
	fw.write("x%d,y%d,z%d" % (t_id, t_id, t_id))
	if t_id==len(tx.keys())-1:
		break
	fw.write(",")
fw.write("\n")

tx_sorted = sorted(tx.keys())
for t_id, t_gene in enumerate(tx_sorted):
	for m1,m2,m3 in tx[t_gene]:
		ss = []
		for tt in range(t_id):
			ss.append("")
			ss.append("")
			ss.append("")
		ss.append("%s" % m1)
		ss.append("%s" % m2)
		ss.append("%s" % m3)
		for tt in range(t_id+1, len(tx.keys())):
			ss.append("")
			ss.append("")
			ss.append("")
		fw.write(",".join(ss) + "\n")	

fw.close()

fw = open(sys.argv[3], "w")

a_1 = """
Plotly.d3.csv("processed.anderson.pos0.by.gene.txt", function(err, rows){
    function unpack(rows, key){
        return rows.map(function(row)
        {return row[key];});
    }

var data = [];
"""

a_2 = ""
for t_id,t_gene in enumerate(tx_sorted):
	a_2 += """
var trace1 = {
    x:unpack(rows, "x%d"), y:unpack(rows, "y%d"), z:unpack(rows, "z%d"),
    mode:"markers",
    name:"%s",
    marker: {color: "rgb(127,127,127)", size:1, symbol:"circle", opacity:0.8,},
    type: "scatter3d"
};
data.push(trace1);
"""	% (t_id+1, t_id+1, t_id+1, t_gene)

a_3 = """
var layout = {
    scene:{
        aspectmode:"manual",
        aspectratio:{x:1,y:1, z:0.05,}
    },
    dragmode:false,
    margin:{l:0, r:0, b:0, t:0},
    };
Plotly.newPlot("myDiv", data, layout, {showSendToCloud:true});
});
"""

fw.write(a_1 + "\n")
fw.write(a_2 + "\n")
fw.write(a_3 + "\n")

fw.close()
