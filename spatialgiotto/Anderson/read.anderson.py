#!/usr/bin/python

import sys
import re
import os

f = open(sys.argv[1])
h = f.readline().rstrip("\n").split(",")[1:]
#sys.stdout.write("gene,x1,y1,z1\n")
while True:
	l = f.readline()
	if l=="": break
	l = l.rstrip("\n")
	ll = l.split(",")
	gene = ll[0]
	cell_id = 0;
	for x in ll[1:]:
		if x=="0":
			cell_id+=1
			continue
		m = re.match("\[(.*)\]", x)
		if m is not None:
			mx = m.group(1).split(";")
			for mi in mx:
				mi_s = mi.split(" ")
				sys.stdout.write("%s,%d,%s,%s,%s\n" % (gene, cell_id, mi_s[0], mi_s[1], mi_s[2]))
		cell_id+=1
f.close()
